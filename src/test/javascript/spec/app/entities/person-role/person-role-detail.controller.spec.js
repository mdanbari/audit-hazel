'use strict';

describe('Controller Tests', function() {

    describe('PersonRole Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockPersonRole, MockPerson, MockRole;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockPersonRole = jasmine.createSpy('MockPersonRole');
            MockPerson = jasmine.createSpy('MockPerson');
            MockRole = jasmine.createSpy('MockRole');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'PersonRole': MockPersonRole,
                'Person': MockPerson,
                'Role': MockRole
            };
            createController = function() {
                $injector.get('$controller')("PersonRoleDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'auditApp:personRoleUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
