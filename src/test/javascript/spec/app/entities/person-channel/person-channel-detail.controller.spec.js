'use strict';

describe('Controller Tests', function() {

    describe('PersonChannel Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockPersonChannel, MockPerson, MockChannel;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockPersonChannel = jasmine.createSpy('MockPersonChannel');
            MockPerson = jasmine.createSpy('MockPerson');
            MockChannel = jasmine.createSpy('MockChannel');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'PersonChannel': MockPersonChannel,
                'Person': MockPerson,
                'Channel': MockChannel
            };
            createController = function() {
                $injector.get('$controller')("PersonChannelDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'auditApp:personChannelUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
