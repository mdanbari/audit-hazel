package net.fava.web.web.rest;

import net.fava.web.AuditApp;

import net.fava.web.domain.PersonChannel;
import net.fava.web.repository.PersonChannelRepository;
import net.fava.web.service.PersonChannelService;
import net.fava.web.repository.search.PersonChannelSearchRepository;
import net.fava.web.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PersonChannelResource REST controller.
 *
 * @see PersonChannelResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AuditApp.class)
public class PersonChannelResourceIntTest {

    private static final String DEFAULT_CHANNEL_DESC = "AAAAAAAAAA";
    private static final String UPDATED_CHANNEL_DESC = "BBBBBBBBBB";

    @Autowired
    private PersonChannelRepository personChannelRepository;

    @Autowired
    private PersonChannelService personChannelService;

    @Autowired
    private PersonChannelSearchRepository personChannelSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPersonChannelMockMvc;

    private PersonChannel personChannel;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PersonChannelResource personChannelResource = new PersonChannelResource(personChannelService);
        this.restPersonChannelMockMvc = MockMvcBuilders.standaloneSetup(personChannelResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PersonChannel createEntity(EntityManager em) {
        PersonChannel personChannel = new PersonChannel()
            .channelDesc(DEFAULT_CHANNEL_DESC);
        return personChannel;
    }

    @Before
    public void initTest() {
        personChannelSearchRepository.deleteAll();
        personChannel = createEntity(em);
    }

    @Test
    @Transactional
    public void createPersonChannel() throws Exception {
        int databaseSizeBeforeCreate = personChannelRepository.findAll().size();

        // Create the PersonChannel
        restPersonChannelMockMvc.perform(post("/api/person-channels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personChannel)))
            .andExpect(status().isCreated());

        // Validate the PersonChannel in the database
        List<PersonChannel> personChannelList = personChannelRepository.findAll();
        assertThat(personChannelList).hasSize(databaseSizeBeforeCreate + 1);
        PersonChannel testPersonChannel = personChannelList.get(personChannelList.size() - 1);
        assertThat(testPersonChannel.getChannelDesc()).isEqualTo(DEFAULT_CHANNEL_DESC);

        // Validate the PersonChannel in Elasticsearch
        PersonChannel personChannelEs = personChannelSearchRepository.findOne(testPersonChannel.getId());
        assertThat(personChannelEs).isEqualToComparingFieldByField(testPersonChannel);
    }

    @Test
    @Transactional
    public void createPersonChannelWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = personChannelRepository.findAll().size();

        // Create the PersonChannel with an existing ID
        personChannel.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPersonChannelMockMvc.perform(post("/api/person-channels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personChannel)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PersonChannel> personChannelList = personChannelRepository.findAll();
        assertThat(personChannelList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkChannelDescIsRequired() throws Exception {
        int databaseSizeBeforeTest = personChannelRepository.findAll().size();
        // set the field null
        personChannel.setChannelDesc(null);

        // Create the PersonChannel, which fails.

        restPersonChannelMockMvc.perform(post("/api/person-channels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personChannel)))
            .andExpect(status().isBadRequest());

        List<PersonChannel> personChannelList = personChannelRepository.findAll();
        assertThat(personChannelList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPersonChannels() throws Exception {
        // Initialize the database
        personChannelRepository.saveAndFlush(personChannel);

        // Get all the personChannelList
        restPersonChannelMockMvc.perform(get("/api/person-channels?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(personChannel.getId().intValue())))
            .andExpect(jsonPath("$.[*].channelDesc").value(hasItem(DEFAULT_CHANNEL_DESC.toString())));
    }

    @Test
    @Transactional
    public void getPersonChannel() throws Exception {
        // Initialize the database
        personChannelRepository.saveAndFlush(personChannel);

        // Get the personChannel
        restPersonChannelMockMvc.perform(get("/api/person-channels/{id}", personChannel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(personChannel.getId().intValue()))
            .andExpect(jsonPath("$.channelDesc").value(DEFAULT_CHANNEL_DESC.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPersonChannel() throws Exception {
        // Get the personChannel
        restPersonChannelMockMvc.perform(get("/api/person-channels/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonChannel() throws Exception {
        // Initialize the database
        personChannelService.save(personChannel);

        int databaseSizeBeforeUpdate = personChannelRepository.findAll().size();

        // Update the personChannel
        PersonChannel updatedPersonChannel = personChannelRepository.findOne(personChannel.getId());
        updatedPersonChannel
            .channelDesc(UPDATED_CHANNEL_DESC);

        restPersonChannelMockMvc.perform(put("/api/person-channels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPersonChannel)))
            .andExpect(status().isOk());

        // Validate the PersonChannel in the database
        List<PersonChannel> personChannelList = personChannelRepository.findAll();
        assertThat(personChannelList).hasSize(databaseSizeBeforeUpdate);
        PersonChannel testPersonChannel = personChannelList.get(personChannelList.size() - 1);
        assertThat(testPersonChannel.getChannelDesc()).isEqualTo(UPDATED_CHANNEL_DESC);

        // Validate the PersonChannel in Elasticsearch
        PersonChannel personChannelEs = personChannelSearchRepository.findOne(testPersonChannel.getId());
        assertThat(personChannelEs).isEqualToComparingFieldByField(testPersonChannel);
    }

    @Test
    @Transactional
    public void updateNonExistingPersonChannel() throws Exception {
        int databaseSizeBeforeUpdate = personChannelRepository.findAll().size();

        // Create the PersonChannel

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPersonChannelMockMvc.perform(put("/api/person-channels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personChannel)))
            .andExpect(status().isCreated());

        // Validate the PersonChannel in the database
        List<PersonChannel> personChannelList = personChannelRepository.findAll();
        assertThat(personChannelList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePersonChannel() throws Exception {
        // Initialize the database
        personChannelService.save(personChannel);

        int databaseSizeBeforeDelete = personChannelRepository.findAll().size();

        // Get the personChannel
        restPersonChannelMockMvc.perform(delete("/api/person-channels/{id}", personChannel.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean personChannelExistsInEs = personChannelSearchRepository.exists(personChannel.getId());
        assertThat(personChannelExistsInEs).isFalse();

        // Validate the database is empty
        List<PersonChannel> personChannelList = personChannelRepository.findAll();
        assertThat(personChannelList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPersonChannel() throws Exception {
        // Initialize the database
        personChannelService.save(personChannel);

        // Search the personChannel
        restPersonChannelMockMvc.perform(get("/api/_search/person-channels?query=id:" + personChannel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(personChannel.getId().intValue())))
            .andExpect(jsonPath("$.[*].channelDesc").value(hasItem(DEFAULT_CHANNEL_DESC.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PersonChannel.class);
    }
}
