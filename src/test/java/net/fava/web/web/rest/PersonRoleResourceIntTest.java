package net.fava.web.web.rest;

import net.fava.web.AuditApp;

import net.fava.web.domain.PersonRole;
import net.fava.web.repository.PersonRoleRepository;
import net.fava.web.service.PersonRoleService;
import net.fava.web.repository.search.PersonRoleSearchRepository;
import net.fava.web.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PersonRoleResource REST controller.
 *
 * @see PersonRoleResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AuditApp.class)
public class PersonRoleResourceIntTest {

    @Autowired
    private PersonRoleRepository personRoleRepository;

    @Autowired
    private PersonRoleService personRoleService;

    @Autowired
    private PersonRoleSearchRepository personRoleSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPersonRoleMockMvc;

    private PersonRole personRole;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PersonRoleResource personRoleResource = new PersonRoleResource(personRoleService);
        this.restPersonRoleMockMvc = MockMvcBuilders.standaloneSetup(personRoleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PersonRole createEntity(EntityManager em) {
        PersonRole personRole = new PersonRole();
        return personRole;
    }

    @Before
    public void initTest() {
        personRoleSearchRepository.deleteAll();
        personRole = createEntity(em);
    }

    @Test
    @Transactional
    public void createPersonRole() throws Exception {
        int databaseSizeBeforeCreate = personRoleRepository.findAll().size();

        // Create the PersonRole
        restPersonRoleMockMvc.perform(post("/api/person-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personRole)))
            .andExpect(status().isCreated());

        // Validate the PersonRole in the database
        List<PersonRole> personRoleList = personRoleRepository.findAll();
        assertThat(personRoleList).hasSize(databaseSizeBeforeCreate + 1);
        PersonRole testPersonRole = personRoleList.get(personRoleList.size() - 1);

        // Validate the PersonRole in Elasticsearch
        PersonRole personRoleEs = personRoleSearchRepository.findOne(testPersonRole.getId());
        assertThat(personRoleEs).isEqualToComparingFieldByField(testPersonRole);
    }

    @Test
    @Transactional
    public void createPersonRoleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = personRoleRepository.findAll().size();

        // Create the PersonRole with an existing ID
        personRole.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPersonRoleMockMvc.perform(post("/api/person-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personRole)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PersonRole> personRoleList = personRoleRepository.findAll();
        assertThat(personRoleList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPersonRoles() throws Exception {
        // Initialize the database
        personRoleRepository.saveAndFlush(personRole);

        // Get all the personRoleList
        restPersonRoleMockMvc.perform(get("/api/person-roles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(personRole.getId().intValue())));
    }

    @Test
    @Transactional
    public void getPersonRole() throws Exception {
        // Initialize the database
        personRoleRepository.saveAndFlush(personRole);

        // Get the personRole
        restPersonRoleMockMvc.perform(get("/api/person-roles/{id}", personRole.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(personRole.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPersonRole() throws Exception {
        // Get the personRole
        restPersonRoleMockMvc.perform(get("/api/person-roles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonRole() throws Exception {
        // Initialize the database
        personRoleService.save(personRole);

        int databaseSizeBeforeUpdate = personRoleRepository.findAll().size();

        // Update the personRole
        PersonRole updatedPersonRole = personRoleRepository.findOne(personRole.getId());

        restPersonRoleMockMvc.perform(put("/api/person-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPersonRole)))
            .andExpect(status().isOk());

        // Validate the PersonRole in the database
        List<PersonRole> personRoleList = personRoleRepository.findAll();
        assertThat(personRoleList).hasSize(databaseSizeBeforeUpdate);
        PersonRole testPersonRole = personRoleList.get(personRoleList.size() - 1);

        // Validate the PersonRole in Elasticsearch
        PersonRole personRoleEs = personRoleSearchRepository.findOne(testPersonRole.getId());
        assertThat(personRoleEs).isEqualToComparingFieldByField(testPersonRole);
    }

    @Test
    @Transactional
    public void updateNonExistingPersonRole() throws Exception {
        int databaseSizeBeforeUpdate = personRoleRepository.findAll().size();

        // Create the PersonRole

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPersonRoleMockMvc.perform(put("/api/person-roles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(personRole)))
            .andExpect(status().isCreated());

        // Validate the PersonRole in the database
        List<PersonRole> personRoleList = personRoleRepository.findAll();
        assertThat(personRoleList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePersonRole() throws Exception {
        // Initialize the database
        personRoleService.save(personRole);

        int databaseSizeBeforeDelete = personRoleRepository.findAll().size();

        // Get the personRole
        restPersonRoleMockMvc.perform(delete("/api/person-roles/{id}", personRole.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean personRoleExistsInEs = personRoleSearchRepository.exists(personRole.getId());
        assertThat(personRoleExistsInEs).isFalse();

        // Validate the database is empty
        List<PersonRole> personRoleList = personRoleRepository.findAll();
        assertThat(personRoleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPersonRole() throws Exception {
        // Initialize the database
        personRoleService.save(personRole);

        // Search the personRole
        restPersonRoleMockMvc.perform(get("/api/_search/person-roles?query=id:" + personRole.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(personRole.getId().intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PersonRole.class);
    }
}
