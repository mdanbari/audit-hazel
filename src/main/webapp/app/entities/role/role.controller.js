(function() {
    'use strict';

    angular
        .module('auditApp')
        .controller('RoleController', RoleController);

    RoleController.$inject = ['Role', 'RoleSearch'];

    function RoleController(Role, RoleSearch) {

        var vm = this;

        vm.roles = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Role.query(function(result) {
                vm.roles = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            RoleSearch.query({query: vm.searchQuery}, function(result) {
                vm.roles = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }    }
})();
