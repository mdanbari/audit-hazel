(function() {
    'use strict';

    angular
        .module('auditApp')
        .factory('PersonChannelSearch', PersonChannelSearch);

    PersonChannelSearch.$inject = ['$resource'];

    function PersonChannelSearch($resource) {
        var resourceUrl =  'api/_search/person-channels/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
