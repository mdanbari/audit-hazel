(function() {
    'use strict';
    angular
        .module('auditApp')
        .factory('PersonChannel', PersonChannel);

    PersonChannel.$inject = ['$resource'];

    function PersonChannel ($resource) {
        var resourceUrl =  'api/person-channels/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
