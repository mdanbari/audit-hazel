(function() {
    'use strict';

    angular
        .module('auditApp')
        .controller('PersonChannelDialogController', PersonChannelDialogController);

    PersonChannelDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'PersonChannel', 'Person', 'Channel'];

    function PersonChannelDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, PersonChannel, Person, Channel) {
        var vm = this;

        vm.personChannel = entity;
        vm.clear = clear;
        vm.save = save;
        vm.people = Person.query();
        vm.channels = Channel.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.personChannel.id !== null) {
                PersonChannel.update(vm.personChannel, onSaveSuccess, onSaveError);
            } else {
                PersonChannel.save(vm.personChannel, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('auditApp:personChannelUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
