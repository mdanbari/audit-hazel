(function() {
    'use strict';

    angular
        .module('auditApp')
        .controller('PersonChannelDetailController', PersonChannelDetailController);

    PersonChannelDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'PersonChannel', 'Person', 'Channel'];

    function PersonChannelDetailController($scope, $rootScope, $stateParams, previousState, entity, PersonChannel, Person, Channel) {
        var vm = this;

        vm.personChannel = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('auditApp:personChannelUpdate', function(event, result) {
            vm.personChannel = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
