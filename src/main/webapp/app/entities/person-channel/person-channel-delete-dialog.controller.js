(function() {
    'use strict';

    angular
        .module('auditApp')
        .controller('PersonChannelDeleteController',PersonChannelDeleteController);

    PersonChannelDeleteController.$inject = ['$uibModalInstance', 'entity', 'PersonChannel'];

    function PersonChannelDeleteController($uibModalInstance, entity, PersonChannel) {
        var vm = this;

        vm.personChannel = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            PersonChannel.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
