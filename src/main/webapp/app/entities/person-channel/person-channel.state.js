(function() {
    'use strict';

    angular
        .module('auditApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('person-channel', {
            parent: 'entity',
            url: '/person-channel?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'PersonChannels'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/person-channel/person-channels.html',
                    controller: 'PersonChannelController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
            }
        })
        .state('person-channel-detail', {
            parent: 'person-channel',
            url: '/person-channel/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'PersonChannel'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/person-channel/person-channel-detail.html',
                    controller: 'PersonChannelDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'PersonChannel', function($stateParams, PersonChannel) {
                    return PersonChannel.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'person-channel',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('person-channel-detail.edit', {
            parent: 'person-channel-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/person-channel/person-channel-dialog.html',
                    controller: 'PersonChannelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PersonChannel', function(PersonChannel) {
                            return PersonChannel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('person-channel.new', {
            parent: 'person-channel',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/person-channel/person-channel-dialog.html',
                    controller: 'PersonChannelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                channelDesc: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('person-channel', null, { reload: 'person-channel' });
                }, function() {
                    $state.go('person-channel');
                });
            }]
        })
        .state('person-channel.edit', {
            parent: 'person-channel',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/person-channel/person-channel-dialog.html',
                    controller: 'PersonChannelDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PersonChannel', function(PersonChannel) {
                            return PersonChannel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('person-channel', null, { reload: 'person-channel' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('person-channel.delete', {
            parent: 'person-channel',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/person-channel/person-channel-delete-dialog.html',
                    controller: 'PersonChannelDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PersonChannel', function(PersonChannel) {
                            return PersonChannel.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('person-channel', null, { reload: 'person-channel' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
