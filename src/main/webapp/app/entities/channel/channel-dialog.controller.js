(function() {
    'use strict';

    angular
        .module('auditApp')
        .controller('ChannelDialogController', ChannelDialogController);

    ChannelDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Channel'];

    function ChannelDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Channel) {
        var vm = this;

        vm.channel = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.channel.id !== null) {
                Channel.update(vm.channel, onSaveSuccess, onSaveError);
            } else {
                Channel.save(vm.channel, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('auditApp:channelUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
