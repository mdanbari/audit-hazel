(function() {
    'use strict';

    angular
        .module('auditApp')
        .controller('ChannelController', ChannelController);

    ChannelController.$inject = ['Channel', 'ChannelSearch'];

    function ChannelController(Channel, ChannelSearch) {

        var vm = this;

        vm.channels = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Channel.query(function(result) {
                vm.channels = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            ChannelSearch.query({query: vm.searchQuery}, function(result) {
                vm.channels = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }    }
})();
