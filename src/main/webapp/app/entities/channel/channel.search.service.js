(function() {
    'use strict';

    angular
        .module('auditApp')
        .factory('ChannelSearch', ChannelSearch);

    ChannelSearch.$inject = ['$resource'];

    function ChannelSearch($resource) {
        var resourceUrl =  'api/_search/channels/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
