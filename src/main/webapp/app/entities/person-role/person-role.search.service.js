(function() {
    'use strict';

    angular
        .module('auditApp')
        .factory('PersonRoleSearch', PersonRoleSearch);

    PersonRoleSearch.$inject = ['$resource'];

    function PersonRoleSearch($resource) {
        var resourceUrl =  'api/_search/person-roles/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
