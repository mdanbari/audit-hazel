(function() {
    'use strict';

    angular
        .module('auditApp')
        .controller('PersonRoleDialogController', PersonRoleDialogController);

    PersonRoleDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'PersonRole', 'Person', 'Role'];

    function PersonRoleDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, PersonRole, Person, Role) {
        var vm = this;

        vm.personRole = entity;
        vm.clear = clear;
        vm.save = save;
        vm.people = Person.query();
        vm.roles = Role.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.personRole.id !== null) {
                PersonRole.update(vm.personRole, onSaveSuccess, onSaveError);
            } else {
                PersonRole.save(vm.personRole, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('auditApp:personRoleUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
