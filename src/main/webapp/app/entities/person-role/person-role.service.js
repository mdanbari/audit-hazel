(function() {
    'use strict';
    angular
        .module('auditApp')
        .factory('PersonRole', PersonRole);

    PersonRole.$inject = ['$resource'];

    function PersonRole ($resource) {
        var resourceUrl =  'api/person-roles/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
