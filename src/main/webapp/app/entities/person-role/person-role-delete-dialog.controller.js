(function() {
    'use strict';

    angular
        .module('auditApp')
        .controller('PersonRoleDeleteController',PersonRoleDeleteController);

    PersonRoleDeleteController.$inject = ['$uibModalInstance', 'entity', 'PersonRole'];

    function PersonRoleDeleteController($uibModalInstance, entity, PersonRole) {
        var vm = this;

        vm.personRole = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            PersonRole.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
