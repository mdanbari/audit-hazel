(function() {
    'use strict';

    angular
        .module('auditApp')
        .controller('PersonRoleDetailController', PersonRoleDetailController);

    PersonRoleDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'PersonRole', 'Person', 'Role'];

    function PersonRoleDetailController($scope, $rootScope, $stateParams, previousState, entity, PersonRole, Person, Role) {
        var vm = this;

        vm.personRole = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('auditApp:personRoleUpdate', function(event, result) {
            vm.personRole = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
