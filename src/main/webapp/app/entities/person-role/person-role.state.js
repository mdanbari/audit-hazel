(function() {
    'use strict';

    angular
        .module('auditApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('person-role', {
            parent: 'entity',
            url: '/person-role?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'PersonRoles'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/person-role/person-roles.html',
                    controller: 'PersonRoleController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
            }
        })
        .state('person-role-detail', {
            parent: 'person-role',
            url: '/person-role/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'PersonRole'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/person-role/person-role-detail.html',
                    controller: 'PersonRoleDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'PersonRole', function($stateParams, PersonRole) {
                    return PersonRole.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'person-role',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('person-role-detail.edit', {
            parent: 'person-role-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/person-role/person-role-dialog.html',
                    controller: 'PersonRoleDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PersonRole', function(PersonRole) {
                            return PersonRole.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('person-role.new', {
            parent: 'person-role',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/person-role/person-role-dialog.html',
                    controller: 'PersonRoleDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('person-role', null, { reload: 'person-role' });
                }, function() {
                    $state.go('person-role');
                });
            }]
        })
        .state('person-role.edit', {
            parent: 'person-role',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/person-role/person-role-dialog.html',
                    controller: 'PersonRoleDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PersonRole', function(PersonRole) {
                            return PersonRole.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('person-role', null, { reload: 'person-role' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('person-role.delete', {
            parent: 'person-role',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/person-role/person-role-delete-dialog.html',
                    controller: 'PersonRoleDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PersonRole', function(PersonRole) {
                            return PersonRole.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('person-role', null, { reload: 'person-role' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
