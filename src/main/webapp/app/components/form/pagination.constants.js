(function() {
    'use strict';

    angular
        .module('auditApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
