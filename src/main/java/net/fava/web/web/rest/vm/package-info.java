/**
 * View Models used by Spring MVC REST controllers.
 */
package net.fava.web.web.rest.vm;
