package net.fava.web.web.rest;

import com.codahale.metrics.annotation.Timed;
import net.fava.web.domain.PersonRole;
import net.fava.web.service.PersonRoleService;
import net.fava.web.web.rest.util.HeaderUtil;
import net.fava.web.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PersonRole.
 */
@RestController
@RequestMapping("/api")
public class PersonRoleResource {

    private final Logger log = LoggerFactory.getLogger(PersonRoleResource.class);

    private static final String ENTITY_NAME = "personRole";
        
    private final PersonRoleService personRoleService;

    public PersonRoleResource(PersonRoleService personRoleService) {
        this.personRoleService = personRoleService;
    }

    /**
     * POST  /person-roles : Create a new personRole.
     *
     * @param personRole the personRole to create
     * @return the ResponseEntity with status 201 (Created) and with body the new personRole, or with status 400 (Bad Request) if the personRole has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/person-roles")
    @Timed
    public ResponseEntity<PersonRole> createPersonRole(@RequestBody PersonRole personRole) throws URISyntaxException {
        log.debug("REST request to save PersonRole : {}", personRole);
        if (personRole.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new personRole cannot already have an ID")).body(null);
        }
        PersonRole result = personRoleService.save(personRole);
        return ResponseEntity.created(new URI("/api/person-roles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /person-roles : Updates an existing personRole.
     *
     * @param personRole the personRole to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated personRole,
     * or with status 400 (Bad Request) if the personRole is not valid,
     * or with status 500 (Internal Server Error) if the personRole couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/person-roles")
    @Timed
    public ResponseEntity<PersonRole> updatePersonRole(@RequestBody PersonRole personRole) throws URISyntaxException {
        log.debug("REST request to update PersonRole : {}", personRole);
        if (personRole.getId() == null) {
            return createPersonRole(personRole);
        }
        PersonRole result = personRoleService.save(personRole);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, personRole.getId().toString()))
            .body(result);
    }

    /**
     * GET  /person-roles : get all the personRoles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of personRoles in body
     */
    @GetMapping("/person-roles")
    @Timed
    public ResponseEntity<List<PersonRole>> getAllPersonRoles(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PersonRoles");
        Page<PersonRole> page = personRoleService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/person-roles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /person-roles/:id : get the "id" personRole.
     *
     * @param id the id of the personRole to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the personRole, or with status 404 (Not Found)
     */
    @GetMapping("/person-roles/{id}")
    @Timed
    public ResponseEntity<PersonRole> getPersonRole(@PathVariable Long id) {
        log.debug("REST request to get PersonRole : {}", id);
        PersonRole personRole = personRoleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(personRole));
    }

    /**
     * DELETE  /person-roles/:id : delete the "id" personRole.
     *
     * @param id the id of the personRole to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/person-roles/{id}")
    @Timed
    public ResponseEntity<Void> deletePersonRole(@PathVariable Long id) {
        log.debug("REST request to delete PersonRole : {}", id);
        personRoleService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/person-roles?query=:query : search for the personRole corresponding
     * to the query.
     *
     * @param query the query of the personRole search 
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/person-roles")
    @Timed
    public ResponseEntity<List<PersonRole>> searchPersonRoles(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of PersonRoles for query {}", query);
        Page<PersonRole> page = personRoleService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/person-roles");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
