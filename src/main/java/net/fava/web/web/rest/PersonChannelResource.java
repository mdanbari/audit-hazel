package net.fava.web.web.rest;

import com.codahale.metrics.annotation.Timed;
import net.fava.web.domain.PersonChannel;
import net.fava.web.service.PersonChannelService;
import net.fava.web.web.rest.util.HeaderUtil;
import net.fava.web.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing PersonChannel.
 */
@RestController
@RequestMapping("/api")
public class PersonChannelResource {

    private final Logger log = LoggerFactory.getLogger(PersonChannelResource.class);

    private static final String ENTITY_NAME = "personChannel";
        
    private final PersonChannelService personChannelService;

    public PersonChannelResource(PersonChannelService personChannelService) {
        this.personChannelService = personChannelService;
    }

    /**
     * POST  /person-channels : Create a new personChannel.
     *
     * @param personChannel the personChannel to create
     * @return the ResponseEntity with status 201 (Created) and with body the new personChannel, or with status 400 (Bad Request) if the personChannel has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/person-channels")
    @Timed
    public ResponseEntity<PersonChannel> createPersonChannel(@Valid @RequestBody PersonChannel personChannel) throws URISyntaxException {
        log.debug("REST request to save PersonChannel : {}", personChannel);
        if (personChannel.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new personChannel cannot already have an ID")).body(null);
        }
        PersonChannel result = personChannelService.save(personChannel);
        return ResponseEntity.created(new URI("/api/person-channels/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /person-channels : Updates an existing personChannel.
     *
     * @param personChannel the personChannel to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated personChannel,
     * or with status 400 (Bad Request) if the personChannel is not valid,
     * or with status 500 (Internal Server Error) if the personChannel couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/person-channels")
    @Timed
    public ResponseEntity<PersonChannel> updatePersonChannel(@Valid @RequestBody PersonChannel personChannel) throws URISyntaxException {
        log.debug("REST request to update PersonChannel : {}", personChannel);
        if (personChannel.getId() == null) {
            return createPersonChannel(personChannel);
        }
        PersonChannel result = personChannelService.save(personChannel);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, personChannel.getId().toString()))
            .body(result);
    }

    /**
     * GET  /person-channels : get all the personChannels.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of personChannels in body
     */
    @GetMapping("/person-channels")
    @Timed
    public ResponseEntity<List<PersonChannel>> getAllPersonChannels(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PersonChannels");
        Page<PersonChannel> page = personChannelService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/person-channels");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /person-channels/:id : get the "id" personChannel.
     *
     * @param id the id of the personChannel to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the personChannel, or with status 404 (Not Found)
     */
    @GetMapping("/person-channels/{id}")
    @Timed
    public ResponseEntity<PersonChannel> getPersonChannel(@PathVariable Long id) {
        log.debug("REST request to get PersonChannel : {}", id);
        PersonChannel personChannel = personChannelService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(personChannel));
    }

    /**
     * DELETE  /person-channels/:id : delete the "id" personChannel.
     *
     * @param id the id of the personChannel to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/person-channels/{id}")
    @Timed
    public ResponseEntity<Void> deletePersonChannel(@PathVariable Long id) {
        log.debug("REST request to delete PersonChannel : {}", id);
        personChannelService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/person-channels?query=:query : search for the personChannel corresponding
     * to the query.
     *
     * @param query the query of the personChannel search 
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/person-channels")
    @Timed
    public ResponseEntity<List<PersonChannel>> searchPersonChannels(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of PersonChannels for query {}", query);
        Page<PersonChannel> page = personChannelService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/person-channels");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }


}
