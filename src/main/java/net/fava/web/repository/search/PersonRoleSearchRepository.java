package net.fava.web.repository.search;

import net.fava.web.domain.PersonRole;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PersonRole entity.
 */
public interface PersonRoleSearchRepository extends ElasticsearchRepository<PersonRole, Long> {
}
