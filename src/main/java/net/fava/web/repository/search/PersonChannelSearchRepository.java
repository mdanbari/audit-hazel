package net.fava.web.repository.search;

import net.fava.web.domain.PersonChannel;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the PersonChannel entity.
 */
public interface PersonChannelSearchRepository extends ElasticsearchRepository<PersonChannel, Long> {
}
