package net.fava.web.repository;

import net.fava.web.domain.PersonChannel;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PersonChannel entity.
 */
@SuppressWarnings("unused")
public interface PersonChannelRepository extends JpaRepository<PersonChannel,Long> {

}
