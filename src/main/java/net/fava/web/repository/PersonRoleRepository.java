package net.fava.web.repository;

import net.fava.web.domain.PersonRole;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PersonRole entity.
 */
@SuppressWarnings("unused")
public interface PersonRoleRepository extends JpaRepository<PersonRole,Long> {

}
