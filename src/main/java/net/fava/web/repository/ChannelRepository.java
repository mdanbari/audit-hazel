package net.fava.web.repository;

import net.fava.web.domain.Channel;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Channel entity.
 */
@SuppressWarnings("unused")
public interface ChannelRepository extends JpaRepository<Channel,Long> {

}
