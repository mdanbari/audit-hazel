package net.fava.web.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A PersonChannel.
 */
@Entity
@Table(name = "person_channel")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "personchannel")
public class PersonChannel extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "channel_desc", nullable = false)
    private String channelDesc;

    @ManyToOne
    private Person person;

    @ManyToOne
    private Channel channel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChannelDesc() {
        return channelDesc;
    }

    public PersonChannel channelDesc(String channelDesc) {
        this.channelDesc = channelDesc;
        return this;
    }

    public void setChannelDesc(String channelDesc) {
        this.channelDesc = channelDesc;
    }

    public Person getPerson() {
        return person;
    }

    public PersonChannel person(Person person) {
        this.person = person;
        return this;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Channel getChannel() {
        return channel;
    }

    public PersonChannel channel(Channel channel) {
        this.channel = channel;
        return this;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PersonChannel personChannel = (PersonChannel) o;
        if (personChannel.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, personChannel.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PersonChannel{" +
            "id=" + id +
            ", channelDesc='" + channelDesc + "'" +
            '}';
    }
}
