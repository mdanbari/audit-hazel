package net.fava.web.service;

import net.fava.web.domain.PersonRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Service Interface for managing PersonRole.
 */
public interface PersonRoleService {

    /**
     * Save a personRole.
     *
     * @param personRole the entity to save
     * @return the persisted entity
     */
    PersonRole save(PersonRole personRole);

    /**
     *  Get all the personRoles.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PersonRole> findAll(Pageable pageable);

    /**
     *  Get the "id" personRole.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PersonRole findOne(Long id);

    /**
     *  Delete the "id" personRole.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the personRole corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PersonRole> search(String query, Pageable pageable);
}
