package net.fava.web.service.impl;

import net.fava.web.service.PersonChannelService;
import net.fava.web.domain.PersonChannel;
import net.fava.web.repository.PersonChannelRepository;
import net.fava.web.repository.search.PersonChannelSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing PersonChannel.
 */
@Service
@Transactional
public class PersonChannelServiceImpl implements PersonChannelService{

    private final Logger log = LoggerFactory.getLogger(PersonChannelServiceImpl.class);
    
    private final PersonChannelRepository personChannelRepository;

    private final PersonChannelSearchRepository personChannelSearchRepository;

    public PersonChannelServiceImpl(PersonChannelRepository personChannelRepository, PersonChannelSearchRepository personChannelSearchRepository) {
        this.personChannelRepository = personChannelRepository;
        this.personChannelSearchRepository = personChannelSearchRepository;
    }

    /**
     * Save a personChannel.
     *
     * @param personChannel the entity to save
     * @return the persisted entity
     */
    @Override
    public PersonChannel save(PersonChannel personChannel) {
        log.debug("Request to save PersonChannel : {}", personChannel);
        PersonChannel result = personChannelRepository.save(personChannel);
        personChannelSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the personChannels.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PersonChannel> findAll(Pageable pageable) {
        log.debug("Request to get all PersonChannels");
        Page<PersonChannel> result = personChannelRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one personChannel by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PersonChannel findOne(Long id) {
        log.debug("Request to get PersonChannel : {}", id);
        PersonChannel personChannel = personChannelRepository.findOne(id);
        return personChannel;
    }

    /**
     *  Delete the  personChannel by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PersonChannel : {}", id);
        personChannelRepository.delete(id);
        personChannelSearchRepository.delete(id);
    }

    /**
     * Search for the personChannel corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PersonChannel> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PersonChannels for query {}", query);
        Page<PersonChannel> result = personChannelSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
