package net.fava.web.service.impl;

import net.fava.web.service.ChannelService;
import net.fava.web.domain.Channel;
import net.fava.web.repository.ChannelRepository;
import net.fava.web.repository.search.ChannelSearchRepository;
import net.fava.web.service.dto.ChannelDTO;
import net.fava.web.service.mapper.ChannelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Channel.
 */
@Service
@Transactional
public class ChannelServiceImpl implements ChannelService{

    private final Logger log = LoggerFactory.getLogger(ChannelServiceImpl.class);
    
    private final ChannelRepository channelRepository;

    private final ChannelMapper channelMapper;

    private final ChannelSearchRepository channelSearchRepository;

    public ChannelServiceImpl(ChannelRepository channelRepository, ChannelMapper channelMapper, ChannelSearchRepository channelSearchRepository) {
        this.channelRepository = channelRepository;
        this.channelMapper = channelMapper;
        this.channelSearchRepository = channelSearchRepository;
    }

    /**
     * Save a channel.
     *
     * @param channelDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ChannelDTO save(ChannelDTO channelDTO) {
        log.debug("Request to save Channel : {}", channelDTO);
        Channel channel = channelMapper.channelDTOToChannel(channelDTO);
        channel = channelRepository.save(channel);
        ChannelDTO result = channelMapper.channelToChannelDTO(channel);
        channelSearchRepository.save(channel);
        return result;
    }

    /**
     *  Get all the channels.
     *  
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ChannelDTO> findAll() {
        log.debug("Request to get all Channels");
        List<ChannelDTO> result = channelRepository.findAll().stream()
            .map(channelMapper::channelToChannelDTO)
            .collect(Collectors.toCollection(LinkedList::new));

        return result;
    }

    /**
     *  Get one channel by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ChannelDTO findOne(Long id) {
        log.debug("Request to get Channel : {}", id);
        Channel channel = channelRepository.findOne(id);
        ChannelDTO channelDTO = channelMapper.channelToChannelDTO(channel);
        return channelDTO;
    }

    /**
     *  Delete the  channel by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Channel : {}", id);
        channelRepository.delete(id);
        channelSearchRepository.delete(id);
    }

    /**
     * Search for the channel corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ChannelDTO> search(String query) {
        log.debug("Request to search Channels for query {}", query);
        return StreamSupport
            .stream(channelSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(channelMapper::channelToChannelDTO)
            .collect(Collectors.toList());
    }
}
