package net.fava.web.service.impl;

import net.fava.web.service.PersonRoleService;
import net.fava.web.domain.PersonRole;
import net.fava.web.repository.PersonRoleRepository;
import net.fava.web.repository.search.PersonRoleSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing PersonRole.
 */
@Service
@Transactional
public class PersonRoleServiceImpl implements PersonRoleService{

    private final Logger log = LoggerFactory.getLogger(PersonRoleServiceImpl.class);
    
    private final PersonRoleRepository personRoleRepository;

    private final PersonRoleSearchRepository personRoleSearchRepository;

    public PersonRoleServiceImpl(PersonRoleRepository personRoleRepository, PersonRoleSearchRepository personRoleSearchRepository) {
        this.personRoleRepository = personRoleRepository;
        this.personRoleSearchRepository = personRoleSearchRepository;
    }

    /**
     * Save a personRole.
     *
     * @param personRole the entity to save
     * @return the persisted entity
     */
    @Override
    public PersonRole save(PersonRole personRole) {
        log.debug("Request to save PersonRole : {}", personRole);
        PersonRole result = personRoleRepository.save(personRole);
        personRoleSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the personRoles.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PersonRole> findAll(Pageable pageable) {
        log.debug("Request to get all PersonRoles");
        Page<PersonRole> result = personRoleRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one personRole by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PersonRole findOne(Long id) {
        log.debug("Request to get PersonRole : {}", id);
        PersonRole personRole = personRoleRepository.findOne(id);
        return personRole;
    }

    /**
     *  Delete the  personRole by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PersonRole : {}", id);
        personRoleRepository.delete(id);
        personRoleSearchRepository.delete(id);
    }

    /**
     * Search for the personRole corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PersonRole> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of PersonRoles for query {}", query);
        Page<PersonRole> result = personRoleSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
