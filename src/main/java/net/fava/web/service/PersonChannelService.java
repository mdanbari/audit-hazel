package net.fava.web.service;

import net.fava.web.domain.PersonChannel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Service Interface for managing PersonChannel.
 */
public interface PersonChannelService {

    /**
     * Save a personChannel.
     *
     * @param personChannel the entity to save
     * @return the persisted entity
     */
    PersonChannel save(PersonChannel personChannel);

    /**
     *  Get all the personChannels.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PersonChannel> findAll(Pageable pageable);

    /**
     *  Get the "id" personChannel.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PersonChannel findOne(Long id);

    /**
     *  Delete the "id" personChannel.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the personChannel corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PersonChannel> search(String query, Pageable pageable);
}
