package net.fava.web.service;

import net.fava.web.service.dto.RequestDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Service Interface for managing Request.
 */
public interface RequestService {

    /**
     * Save a request.
     *
     * @param requestDTO the entity to save
     * @return the persisted entity
     */
    RequestDTO save(RequestDTO requestDTO);

    /**
     *  Get all the requests.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<RequestDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" request.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    RequestDTO findOne(Long id);

    /**
     *  Delete the "id" request.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the request corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<RequestDTO> search(String query, Pageable pageable);
}
