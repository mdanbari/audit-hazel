package net.fava.web.service.mapper;

import net.fava.web.domain.*;
import net.fava.web.service.dto.ChannelDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Channel and its DTO ChannelDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ChannelMapper {

    ChannelDTO channelToChannelDTO(Channel channel);

    List<ChannelDTO> channelsToChannelDTOs(List<Channel> channels);

    Channel channelDTOToChannel(ChannelDTO channelDTO);

    List<Channel> channelDTOsToChannels(List<ChannelDTO> channelDTOs);
    /**
     * generating the fromId for all mappers if the databaseType is sql, as the class has relationship to it might need it, instead of
     * creating a new attribute to know if the entity has any relationship from some other entity
     *
     * @param id id of the entity
     * @return the entity instance
     */
     
    default Channel channelFromId(Long id) {
        if (id == null) {
            return null;
        }
        Channel channel = new Channel();
        channel.setId(id);
        return channel;
    }
    

}
