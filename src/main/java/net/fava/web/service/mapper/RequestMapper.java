package net.fava.web.service.mapper;

import net.fava.web.domain.*;
import net.fava.web.service.dto.RequestDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Request and its DTO RequestDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RequestMapper {

    RequestDTO requestToRequestDTO(Request request);

    List<RequestDTO> requestsToRequestDTOs(List<Request> requests);

    Request requestDTOToRequest(RequestDTO requestDTO);

    List<Request> requestDTOsToRequests(List<RequestDTO> requestDTOs);
    /**
     * generating the fromId for all mappers if the databaseType is sql, as the class has relationship to it might need it, instead of
     * creating a new attribute to know if the entity has any relationship from some other entity
     *
     * @param id id of the entity
     * @return the entity instance
     */
     
    default Request requestFromId(Long id) {
        if (id == null) {
            return null;
        }
        Request request = new Request();
        request.setId(id);
        return request;
    }
    

}
