package net.fava.web.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import net.fava.web.domain.enumeration.SentResultStatus;

/**
 * A DTO for the Request entity.
 */
public class RequestDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    @NotNull
    private String sender;

    @NotNull
    private String receiver;

    @NotNull
    @Size(max = 5000)
    private String metadata;

    @NotNull
    private String result;

    @NotNull
    private String channel;

    private Integer retryCount;

    private ZonedDateTime requestCreatedDate;

    private ZonedDateTime lastRetryDate;

    private ZonedDateTime sentDate;

    private SentResultStatus sentResultStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }
    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
    public Integer getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }
    public ZonedDateTime getRequestCreatedDate() {
        return requestCreatedDate;
    }

    public void setRequestCreatedDate(ZonedDateTime requestCreatedDate) {
        this.requestCreatedDate = requestCreatedDate;
    }
    public ZonedDateTime getLastRetryDate() {
        return lastRetryDate;
    }

    public void setLastRetryDate(ZonedDateTime lastRetryDate) {
        this.lastRetryDate = lastRetryDate;
    }
    public ZonedDateTime getSentDate() {
        return sentDate;
    }

    public void setSentDate(ZonedDateTime sentDate) {
        this.sentDate = sentDate;
    }
    public SentResultStatus getSentResultStatus() {
        return sentResultStatus;
    }

    public void setSentResultStatus(SentResultStatus sentResultStatus) {
        this.sentResultStatus = sentResultStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RequestDTO requestDTO = (RequestDTO) o;

        if ( ! Objects.equals(id, requestDTO.id)) { return false; }

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "RequestDTO{" +
            "id=" + id +
            ", sender='" + sender + "'" +
            ", receiver='" + receiver + "'" +
            ", metadata='" + metadata + "'" +
            ", result='" + result + "'" +
            ", channel='" + channel + "'" +
            ", retryCount='" + retryCount + "'" +
            ", requestCreatedDate='" + requestCreatedDate + "'" +
            ", lastRetryDate='" + lastRetryDate + "'" +
            ", sentDate='" + sentDate + "'" +
            ", sentResultStatus='" + sentResultStatus + "'" +
            '}';
    }
}
